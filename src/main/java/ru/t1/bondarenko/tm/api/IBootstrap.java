package ru.t1.bondarenko.tm.api;

public interface IBootstrap {

    void run(String... args);

}
