package ru.t1.bondarenko.tm.api;

public interface ICommandController {

    void showCommandError();

    void showArgumentError();

    void showWelcome();

    void showDeveloperInfo();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

}
