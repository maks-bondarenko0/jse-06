package ru.t1.bondarenko.tm.api;

import ru.t1.bondarenko.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
