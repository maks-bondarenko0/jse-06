package ru.t1.bondarenko.tm.component;

import ru.t1.bondarenko.tm.api.IBootstrap;
import ru.t1.bondarenko.tm.api.ICommandController;
import ru.t1.bondarenko.tm.api.ICommandRepository;
import ru.t1.bondarenko.tm.api.ICommandService;
import ru.t1.bondarenko.tm.constant.ArgumentConstant;
import ru.t1.bondarenko.tm.constant.TerminalConstant;
import ru.t1.bondarenko.tm.controller.CommandController;
import ru.t1.bondarenko.tm.repository.CommandRepository;
import ru.t1.bondarenko.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    @Override
    public void run(final String... args) {
        parseArguments(args);
        parseCommands();
    }

    private void parseCommands() {
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.next();
            parseCommand(command);
        }
    }

    private void exit() {
        System.exit(0);
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showArgumentError();
        }
        exit();
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case TerminalConstant.VERSION:
                commandController.showVersion();
                break;
            case TerminalConstant.HELP:
                commandController.showHelp();
                break;
            case TerminalConstant.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConstant.EXIT:
                exit();
                break;
            default:
                commandController.showCommandError();
        }
    }

}
