package ru.t1.bondarenko.tm.repository;

import ru.t1.bondarenko.tm.api.ICommandRepository;
import ru.t1.bondarenko.tm.constant.ArgumentConstant;
import ru.t1.bondarenko.tm.constant.TerminalConstant;
import ru.t1.bondarenko.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConstant.INFO,
            "Show developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConstant.VERSION,
            "Show application version."
    );

    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConstant.HELP,
            "Show application commands."
    );

    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, ArgumentConstant.COMMANDS,
            "Show application commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS,
            "Show application arguments."
    );

    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null,
            "Close application."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, VERSION, HELP, COMMANDS, ARGUMENTS, EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    };

}
